package {

    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.media.SoundTransform;

    public class SND {

        // Звуки берутся из общей библиотеки
        // Не стал выносить в подгружаемые либы. Упаковал в прилинковываемую SWC вместе с графикой.
        public static const BTN_CLICK:Class    = buySound;
        public static const SLOT_STOP:Class    = stopSpin2;
        public static const SLOT_SPINNED:Class = spinned;
        public static const SLOT_SET:Class     = flip;

        private static var _instance:SND            = new SND();
        private var _volumeTransform:SoundTransform = new SoundTransform();

        private var _backSound:Sound = new SLOT_SPINNED();
        private var _backChannel:SoundChannel = new SoundChannel();

        public function SND() {
            trace( '∙ SND.SND' );
            if ( _instance ) {
                throw new Error( 'SND.SINGLETON!' );
            }
            _volumeTransform.volume = Settings.VOLUME;
        }

        public static function playSound( className:Class ):void {
            var sound:Sound = new className();
            sound.play( 0, 0, _instance._volumeTransform );
        }

        public static function playBackGround():void {
            if ( !Settings.MUTE ) {
                stopBackGround();
                _instance._backChannel = _instance._backSound.play( 0, 1000, _instance._volumeTransform );
            }
        }

        public static function stopBackGround():void {
            if ( _instance._backChannel ){
                _instance._backChannel.stop();
            }
        }

    }
}
