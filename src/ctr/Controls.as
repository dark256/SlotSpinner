package ctr {

    import flash.events.MouseEvent;

    import ui.Buttons.GameButton;
    import ui.Spinners.SpinnerSet;

    public class Controls {

        private static var _startButton:GameButton;
        private static var _stopButton:GameButton;
        private static var _maskBtn:GameButton;
        private static var _masked:Boolean = true;

        public static function init( mainScreen:MainScreen ):void {
            _startButton = new GameButton( mainScreen.playBtn, 'START', onButtonStartClick );
            _stopButton  = new GameButton( mainScreen.stopBtn, 'STOP', onButtonStopClick );
            _stopButton.disable();

            if ( Settings._DEBUG && false ) {
                _maskBtn = new GameButton( mainScreen.maskBtn, 'MASK', onButtonMaskClick )
            } else {
                mainScreen.maskBtn.visible = false;
            }
        }

        private static function onButtonStartClick( event:MouseEvent ):void {
            _startButton.disable();
            _stopButton.enable();
            SpinnerSet.runSpinners();
        }

        private static function onButtonStopClick( event:MouseEvent ):void {
            _stopButton.disable();
            SpinnerSet.stopSpinners( UTILS.getRandomSlotStopSequence() );
        }

        public static function enableStart():void {
            _startButton.enable();
        }

        private static function onButtonMaskClick( event:MouseEvent ):void {
            SpinnerSet.setMask( !_masked );
            _masked = !_masked;
        }


    }
}
