/**
 * Created by Root on 05.11.17.
 */
package {

    import flash.display.Stage;

    public class Settings {

        public static const VOLUME:Number = 0.3;          //Громкость
        public static const MUTE:Boolean  = true;

        public static var stage:Stage;
        public static var _DEBUG:Boolean = true;

        public static const SPINNER_FRAMES_AMOUNT:int          = 10;      // Кол-во фрэймов в барабане
        public static const SPINNER_SPEED:int                  = 45;
        public static const SPINNERS_TEMPLATES:Vector.<String> = new <String>[ '1243215214', '3521421421', '5123244212', '2133412315', '3542134213' ];

    }
}
