package {

    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.Event;

    import ui.Spinners.SpinnerSet;
    import ctr.Controls;

    [Frame(factoryClass="Preloader")]
    [SWF(width='600', height='500', backgroundColor='0x440000', frameRate='60')]

    public class Main extends Sprite {

        private var _mainScreen:MainScreen;
        private var _stats:DisplayObject;

        public function Main() {
            if ( stage ) {
                initGame();
            } else {
                addEventListener( Event.ADDED_TO_STAGE, onAddToStage );
            }
        }

        private function onAddToStage( e:Event ):void {
            Settings.stage = stage;
            removeEventListener( Event.ADDED_TO_STAGE, onAddToStage );
            stage.addEventListener( Event.RESIZE, onResize );
            initGame();
        }

        private function onResize( e:Event ):void {
            if ( _stats )_stats.x = stage.stageWidth - _stats.width;
            _mainScreen.x = (stage.stageWidth - _mainScreen.back.width)/2;
            _mainScreen.y = (stage.stageHeight - _mainScreen.back.height)/2;
        }

        private function initGame():void {
            trace( '■ Main.initGame' );
            SND.playSound( SND.SLOT_SET );

            _mainScreen = addChild( new MainScreen() ) as MainScreen;
            _stats = addChild( new Stats() );

            Controls.init( _mainScreen );
            SpinnerSet.init( _mainScreen );

            onResize( null );
        }
    }
}
