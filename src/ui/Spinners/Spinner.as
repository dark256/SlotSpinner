/**
 * Created by Root on 03.11.17.
 */
package ui.Spinners {

    import ui.Windows.ErrorWindow;

    import com.greensock.TweenLite;
    import com.greensock.easing.Elastic;

    import flash.display.Sprite;
    import flash.events.Event;

    public class Spinner {

        private var _target:slotPlace;
        private var _mask:Sprite;
        private var _lenta:Sprite;


        private var _slotFrameHeight:int = 100;                                     // Высота фрэйма барабана
        private var _signatures:String;                                             // Символы фрэймов барабана

        private var _lentaHeight:int     = ( Settings.SPINNER_FRAMES_AMOUNT ) * _slotFrameHeight;
        private var _onSpinnerStop:Function;
        private var _stopPoint:int;


        public function Spinner( target:slotPlace, signatures:String, onSpinnerStop:Function ) {
            _onSpinnerStop = onSpinnerStop;
            _signatures = signatures;
            _target        = target;
            _lenta         = _target.framesContainer;

            if ( signatures.length != Settings.SPINNER_FRAMES_AMOUNT ){
                new ErrorWindow( 'Wrong _spinnerFrames length:'+signatures.length, ' sinature: '+signatures );
                return;
            }

            init();
        }

        private function init():void {
            var slot:slotFrame;

            //Формируем последовательность символов для барабана с "перехлёстом"
            var tmpSigns:String = _signatures.substr( _signatures.length-2,2)+_signatures+_signatures.substr(0,3);

            for ( var i:int = -2; i < Settings.SPINNER_FRAMES_AMOUNT + 3; i++ ) {
                slot   = _lenta.addChild( new slotFrame() ) as slotFrame;
                slot.y = (i) * _slotFrameHeight;

                slot.frameId.text = i.toString();
                slot.num.text = tmpSigns.substr( i+2,1 );
            }

            _mask              = _target.addChild( new slotBack() ) as Sprite;
            _mask.mouseEnabled = false;
            _lenta.cacheAsBitmap = true;
            setMask( true );

        }

        public function setMask( flag:Boolean ):void {
            //trace( '∙ Spinner.setMask' );
            if ( flag ) {
                _lenta.mask = _mask;
                _target.scaleY = 1;
                _mask.visible = true;
            } else {
                _lenta.mask = null;
                _target.scaleY = 0.5;
                _mask.visible = false;
            }
        }

        public function run():void {
            _stopPoint = -1;
            _lenta.addEventListener( Event.ENTER_FRAME, onEnterFrame );
        }

        private function onEnterFrame( event:Event ):void {
            _lenta.y -= Settings.SPINNER_SPEED + UTILS.RND( 5 );
            if ( _lenta.y <= -_lentaHeight ) {
                _lenta.y = _lenta.y + _lentaHeight;
            }
            var scrollIndex:int = -1 * Math.round( _lenta.y / _slotFrameHeight );
            if ( scrollIndex == Settings.SPINNER_FRAMES_AMOUNT ) {
                scrollIndex = 0;
            }

            if ( _stopPoint != -1 && _stopPoint == scrollIndex ) {
                _lenta.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
                stop();
            }
        }

        private function lentaYPosition( frameNumber:int ):Number {
            return -(frameNumber - 1) * _slotFrameHeight;
        }

        public function stopAt( pos:int ):void {
            trace( '∙ Spinner.stopAt', pos );
            _stopPoint = pos;
        }

        public function setPosTo( pos:int ):void {
            _lenta.y        = lentaYPosition( pos );
        }

        public function stop():void {
            SND.playSound( SND.SLOT_STOP );
            TweenLite.to( _lenta, 0.6, {y: lentaYPosition( _stopPoint ), ease: Elastic.easeOut, onComplete: onSpinnerStop} );
        }

        private function onSpinnerStop():void {
            _onSpinnerStop();
        }
    }
}
