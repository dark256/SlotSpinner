/**
 * Created by Root on 03.11.17.
 */
package ui.Spinners {

    import ctr.Controls;

    public class SpinnerSet {

        private static var _amount:int                = 5;
        private static var _spinners:Vector.<Spinner> = new Vector.<Spinner>();
        private static var _spinnerPointer:int;
        private static var _seq:Array                 = [];
        private static var _container:MainScreen;


        public static function init( container:MainScreen ):void {
            trace( '■■■ SpinnerSet.init' );

            _container = container;
            for ( var s:int = 1; s <= _amount; s++ ) {
                _spinners.push( new Spinner( container[ 'slot' + s ], Settings.SPINNERS_TEMPLATES[ s - 1 ], onSpinnerStop ) );
                _spinners[ s - 1 ].setPosTo( UTILS.RND( Settings.SPINNER_FRAMES_AMOUNT ) );
            }
        }

        public static function runSpinners():void {
            SND.playBackGround();
            _spinnerPointer = 0;
            for ( var i:int = 0; i < _spinners.length; i++ ) {
                _spinners[ i ].run();
            }
        }

        public static function stopSpinners( seq:String ):void {
            _container.targetSet.text = 'TargetSet: ' + seq + ' // ' + UTILS.decodeSpinnerSequence( seq );
            _seq                      = seq.split( '' );
            stopSpinner();
        }

        private static function stopSpinner():void {
            _spinners[ _spinnerPointer ].stopAt( Number( _seq[ _spinnerPointer ] ) );
            _spinnerPointer++;
        }

        private static function onSpinnerStop():void {
            if ( _spinnerPointer < _amount ) {
                stopSpinner();
            } else {
                SND.stopBackGround();
                Controls.enableStart();
            }
        }

        // Тестовая фича
        public static function setMask( flag:Boolean ):void {
            for ( var i:int = 0; i < _spinners.length; i++ ) {
                _spinners[ i ].setMask( flag );
            }
        }

    }
}
