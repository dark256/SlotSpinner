/**
 * Created by Root on 05.11.17.
 */
package ui.Windows {

    public class ErrorWindow extends MessageWindow {
        public function ErrorWindow( msg:String, extraMsg:String = '', OnCallBack:Function = null ) {
            super( msg, extraMsg, OnCallBack );
            _button.view      = false;
            _closeButton.view = false;
        }
    }
}
