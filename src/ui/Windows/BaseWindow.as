package ui.Windows {

    import com.greensock.TweenLite;
    import com.greensock.easing.Back;

    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import ui.Buttons.GameButton;

    public class BaseWindow extends MovieClip{

        private var parentDo:DisplayObjectContainer;
        private var _winbar:WindowsBar;
        public var _closeButton:GameButton;
        private var _infoButton:GameButton;
        private var _lock:Boolean;

        private static var _wb:MovieClip;
        private static var _winCount:int = 0;

        public var baseWin:MovieClip;

        /**
         *  Created by dark256 on 14.02.17.
         * baseWin = win;
         * super(p, false);
         * @param p - DisplayObjectContainer
         * @param lock - вкл выкл блокирующую подложку окна
         */

        public function BaseWindow( p:DisplayObjectContainer, lock:Boolean = true ) {

            parentDo = p;
            _lock = lock;

            _winCount++;
            baseWin.addEventListener( Event.ADDED_TO_STAGE, OnAddToStage );

            if ( baseWin.winBar ) {
                _winbar = new WindowsBar( baseWin.winBar );
            } else {
                _winbar = new WindowsBar( baseWin.back, false );
            }

            if ( baseWin.close ) {
                baseWin.close.addEventListener( MouseEvent.CLICK, OnCloseClick );
                _closeButton  = new GameButton( baseWin.close, 'CLOSE', OnCloseClick );
            }

            if ( baseWin.infoButton ) {
                baseWin.infoButton.addEventListener( MouseEvent.CLICK, OnHelpClick );
                _infoButton  = new GameButton( baseWin.infoButton, 'INFO', OnHelpClick );
            }
            if ( _lock && !_wb ) {
                _wb = new win_back();
                _wb.alpha = 0;
                parentDo.addChild( _wb );
            }

            baseWin.alpha = 0;
            parentDo.addChild( baseWin );
        }

        public function alignText():void{
            if ( baseWin.desc ) {
                baseWin.desc.y = baseWin.back.y + ( baseWin.back.height - baseWin.desc.height) / 2;
            }
        }

        public function updateView():void {
            trace( '∙ BaseWindow.updateView' );
        }

        public function OnHelpClick(e:Event ):void{
            var msg:String = 'Подсказка на стадии создания и внедрения.\n08.MAR.2017';
            //parentDo.addChild( new HelpWindow( parentDo, msg ) );
        }

        public function OnAddToStage( e:Event ):void {
            //SND.playSound(SND.WINDOW_EXPAND);
            baseWin.removeEventListener( Event.ADDED_TO_STAGE, OnAddToStage );
            Settings.stage.addEventListener( Event.RESIZE, OnResize );
            OnResize( null );
            baseWin.y += 170;
            TweenLite.to( baseWin, 0.5, {'y': baseWin.y - 170, 'alpha': 1, ease: Back.easeOut, onComplete: onComplete} );
            if ( _wb ) {
                TweenLite.to( _wb, 0.3, {'alpha': 1} );
            }
        }

        public function onComplete():void { }

        public function OnResize( e:Event ):void {
            baseWin.x = Math.round( Settings.stage.stageWidth / 2 - baseWin.back.width / 2 );
            baseWin.y = Math.round( Settings.stage.stageHeight / 2 - baseWin.back.height / 2 );
            if ( _lock ) {
                _wb.width  = Settings.stage.stageWidth;
                _wb.height = Settings.stage.stageHeight;
            }
        }

        public function OnCloseClick( e:MouseEvent ):void {
            TweenLite.to( baseWin, 0.3, {'y': baseWin.y + 150, 'alpha': 0, ease: Back.easeIn, onComplete: onCompleteClose} );
            if ( _wb && _winCount == 1 ) {
                TweenLite.to( _wb, 0.3, {'alpha': 0} );
            }
            //SND.playSound(SND.WINDOW_COLLAPSE);
        }

        public function onCompleteClose():void{

            if ( Settings.stage ) {
                Settings.stage.removeEventListener( Event.RESIZE, OnResize );
            }

            _winbar.destroy();

            _winCount--;

            if ( _wb && _winCount == 0 ){
                parentDo.removeChild( _wb );
                _wb = null;
            }
            if (baseWin){
                parentDo.removeChild( baseWin );
            }
            if ( baseWin.close ) {
                _closeButton.destroyButton();
            }
            if ( baseWin.infoButton ) {
                _infoButton.destroyButton();
            }

            if ( this.parent ) {
                this.parent.removeChild( this );
            }

            parentDo = null;

        }
    }
}
