package ui.Windows {

    /**
     * @author dark256
     */

    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;

    import ui.Buttons.GameButton;

    public class MessageWindow extends BaseWindow {

        private var OnCallBack:Function;
        public var _button:GameButton;

        public function MessageWindow( msg:String, extraMsg:String = '', OnCallBack:Function = null ) {

            this.OnCallBack = OnCallBack;
            baseWin         = new message_win();

            baseWin.desc.wordWrap     = true;
            baseWin.desc.autoSize     = TextFieldAutoSize.LEFT;
            baseWin.desc.text         = msg;
            baseWin.desc.mouseEnabled = false;
            alignText();

            baseWin.errtxt.text         = extraMsg;
            baseWin.errtxt.mouseEnabled = false;

            _button = new GameButton( baseWin.btn, 'OK', OnBtnClick);

            super( Settings.stage );
        }

        override public function alignText():void {
        }

        public function OnBtnClick( e:MouseEvent ):void {
            if ( OnCallBack != null ) {
                OnCallBack();
            }
            OnCloseClick( null );
        }

        override public function OnCloseClick( e:MouseEvent ):void {
            super.OnCloseClick( e );
            _button.destroyButton();
        }
    }
}