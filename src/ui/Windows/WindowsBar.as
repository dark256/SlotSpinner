
package ui.Windows {

import flash.display.DisplayObject;
import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.ui.Mouse;
    import flash.ui.MouseCursor;



    /**
     * Добавляем функционал для перемещения окон со встроенной плашки-клипа 'winBar'
     * Created by dark256 on 09.02.17.
     *
     * Добавляем
     * import ui.Windows.WindowsBar;
     * private var bar:ui.Windows.WindowsBar;
     * bar = new ui.Windows.WindowsBar( win.winBar );
     * bar.destroy();
     */
    public class WindowsBar {

        private var _winBar:DisplayObject;
        private var _hide:Boolean;
        private var droppedObject:MovieClip;

        public function WindowsBar( winBar:DisplayObject, hide:Boolean = true ) {
            if ( winBar ) {
                _winBar = winBar;
                _hide = hide;

                if ( _hide ) {
                    winBar.alpha = 0;
                    winBar.addEventListener( MouseEvent.MOUSE_OVER, OnPageTextOver );
                    winBar.addEventListener( MouseEvent.MOUSE_OUT, OnPageTextOut );
                }
                winBar.addEventListener( MouseEvent.MOUSE_DOWN, OnWinBarMouseDown );
                winBar.addEventListener( MouseEvent.MOUSE_UP, OnWinBarMouseUp );

                Settings.stage.addEventListener( Event.MOUSE_LEAVE, handleMouseUp );
            }
        }

        private static function OnPageTextOut( e:MouseEvent ):void {Mouse.cursor = MouseCursor.AUTO;}

        private static function OnPageTextOver( e:MouseEvent ):void {Mouse.cursor = MouseCursor.BUTTON;}

        private function OnWinBarMouseDown( e:MouseEvent ):void {
            e.currentTarget.parent.startDrag( false );
            droppedObject = e.currentTarget.parent;
            _winBar.parent.parent.setChildIndex( _winBar.parent, _winBar.parent.parent.numChildren - 1 );
        }

        private function OnWinBarMouseUp( e:MouseEvent ):void {
            e.currentTarget.parent.stopDrag();
            droppedObject = null;
        }

        private function handleMouseUp( e:Event ):void{
            if ( droppedObject ){
                droppedObject.stopDrag();
                droppedObject = null;
            }
        }

        public function destroy():void{
            if ( _winBar ) {
                if ( _hide ) {
                    _winBar.removeEventListener( MouseEvent.MOUSE_OVER, OnPageTextOver );
                    _winBar.removeEventListener( MouseEvent.MOUSE_OUT, OnPageTextOut );
                }
                _winBar.removeEventListener( MouseEvent.MOUSE_DOWN, OnWinBarMouseDown );
                _winBar.removeEventListener( MouseEvent.MOUSE_UP, OnWinBarMouseUp );
                Settings.stage.removeEventListener( Event.MOUSE_LEAVE, handleMouseUp );
            }
        }
    }
}
