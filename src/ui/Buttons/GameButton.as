package ui.Buttons {

    import flash.display.MovieClip;
    import flash.events.MouseEvent;

    public class GameButton {

        private static const UP_BUTTON_STATE:int       = 1;
        private static const OVER_BUTTON_STATE:int     = 2;
        private static const DOWN_BUTTON_STATE:int     = 5;
        private static const DISABLED_BUTTON_STATE:int = 3;

        private var _selected:Boolean = false;
        private var _disabled:Boolean = false;
        private var _callBack:Function;
        private var _buttonClip:MovieClip;

        public function GameButton( buttonClip:MovieClip, buttonLabel:String, onButtonClickCallBack:Function ) {
            _callBack = onButtonClickCallBack;
            _buttonClip      = buttonClip;

            _buttonClip.mouseChildren = false;
            _buttonClip.buttonMode    = true;

            if ( _buttonClip.caption ) {
                _buttonClip.caption.mouseEnabled = false;
                _buttonClip.caption.text         = buttonLabel;
            }
            initButton();
            enable();
        }


        public function set view( flag:Boolean ):void{
            _buttonClip.visible = flag;
        }

        private function initButton():void {
            _buttonClip.addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
            _buttonClip.addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
            _buttonClip.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
            _buttonClip.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
        }

        public function select():void {
            _selected = true;
            _buttonClip.gotoAndStop( OVER_BUTTON_STATE );
        }

        public function deSelect():void {
            _selected = false;
            _buttonClip.gotoAndStop( UP_BUTTON_STATE );
        }

        public function isEnable():Boolean {
            return !_disabled && !_selected;
        }

        public function enable():void {
            _disabled = false;
            _buttonClip.mouseEnabled = true;
            _buttonClip.gotoAndStop( UP_BUTTON_STATE );
        }

        public function disable():void {
            _disabled = true;
            _buttonClip.mouseEnabled = false;
            _buttonClip.gotoAndStop( DISABLED_BUTTON_STATE );
        }

        private function onMouseOver( e:MouseEvent ):void {
            if ( !_disabled ) {
                _buttonClip.gotoAndStop( OVER_BUTTON_STATE );
            } else {
                _buttonClip.gotoAndStop( DISABLED_BUTTON_STATE );
            }
        }

        private function onMouseOut( e:MouseEvent ):void {
            if ( !_disabled ) {
                if ( _selected ) {
                    _buttonClip.gotoAndStop( OVER_BUTTON_STATE );
                } else {
                    _buttonClip.gotoAndStop( UP_BUTTON_STATE );
                }
            } else {
                _buttonClip.gotoAndStop( DISABLED_BUTTON_STATE );
            }
        }

        private function onMouseDown( e:MouseEvent ):void {
            if ( !_selected && !_disabled ) {
                _buttonClip.gotoAndStop( DOWN_BUTTON_STATE );
            }
        }

        private function onMouseUp( e:MouseEvent ):void {
            if ( !_disabled ) {
                if ( _selected ) {
                    _buttonClip.gotoAndStop( OVER_BUTTON_STATE );
                } else {
                    SND.playSound( SND.BTN_CLICK );
                    _buttonClip.gotoAndStop( UP_BUTTON_STATE );
                    if ( _callBack != null ) {
                        _callBack( e );
                    }
                }
            }
        }

        public function destroyButton():void {
            _buttonClip.removeEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
            _buttonClip.removeEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
            _buttonClip.removeEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
            _buttonClip.removeEventListener( MouseEvent.MOUSE_UP, onMouseUp );
        }
    }
}