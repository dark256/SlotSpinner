package {

    import flash.display.DisplayObject;
    import avmplus.getQualifiedClassName;

    public class UTILS {



        public static function whereAmI( place:* ):void {
            trace( '---------where Am I start--------' );
            var muhParent:DisplayObject = place as DisplayObject;
            while (muhParent) {
                trace( '\t∙', muhParent, muhParent.name, getQualifiedClassName( muhParent ) );
                muhParent = muhParent.parent;
            }
            trace( '---------where Am I end--------' );
        }

        public static function localCheck():Boolean {
            return ( Settings.stage.loaderInfo.url.substring( 0, 6 ) == "file:/" ? true : false );
        }

        public static function RND( num:int ):int{
            return Math.round( Math.random()*num );
        }

        public static function getRandomSlotStopSequence():String {
            var rez:String = '';
            for ( var j:int =0; j<5; j++ ){
                rez += RND( Settings.SPINNER_FRAMES_AMOUNT-1 ).toString();
            }
            return rez;
        }

        public static function decodeSpinnerSequence( seq:String ):String {
            var rez:String = '';
            for ( var i:int = 0; i < seq.length; i++ ) {
                rez += Settings.SPINNERS_TEMPLATES[ i ].substr( Number( seq.substr( i, 1 ) ), 1 );
            }
            return rez
        }

    }
}